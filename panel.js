﻿chrome.devtools.network.onRequestFinished.addListener(request => {
  request.getContent((body) => {
      handleBody(body);
  });
});

var storedData = {};
var membersInClan = 0;
var maxBuildings = 0;
var guildMembers = [];
var currentUserName = '';

render();

function handleBody(body) {
    try {
        var json = JSON.parse(body)
    } catch (e) {
        return;
    }
    
    if(!Array.isArray(json)) {
        return;
    }
    
    let guildBuildings = [];
    let methodFound = false;
    
    json.forEach((message) => {
        if(message === null) {
            return;
        }
        if(message.requestClass === 'CityMapService' && message.requestMethod === 'getEntities') {
            message.responseData.forEach((entity) => {
                let entityParsed = parseEntity(entity);
                if(Object.keys(entityParsed).length === 0) {
                    return;
                }
                guildBuildings.push(entityParsed);
            });    
            storedData[currentUserName] = guildBuildings;
            render();
            return;
        }
        if(message.requestClass === 'StartupService' && message.requestMethod === 'getData') {
            currentUserName = message.responseData.user_data.user_name;
            
            if(isClanList(message.responseData.socialbar_list)) {
                fillClan(message.responseData.socialbar_list);
            }
            
            render();
            return;
        }

        if(message.requestClass !== 'OtherPlayerService') {
            return;
        }
        
        if(message.requestMethod === 'getClanMemberList') {
            fillClan(message.responseData);
            return;
        }
        
        if(message.requestMethod !== 'visitPlayer') {
            return;
        }
        if(!message.responseData.other_player.is_guild_member) {
            return;
        }
        membersInClan = message.responseData.other_player.clan.membersNum;
        methodFound = true;
        message.responseData.city_map.entities.forEach((entity) => {
            let entityParsed = parseEntity(entity);
            
            if(Object.keys(entityParsed).length === 0) {
                return;
            }
            guildBuildings.push(entityParsed);
        });
        storedData[message.responseData.other_player.name] = guildBuildings;
    });
    

    render();
}

function isClanList(users) {
    let isClan = true;
    users.forEach((user) => {
        isClan = isClan && user.is_guild_member;
    });
    return isClan;
}

function fillClan(users) {
    guildMembers = [];
    users.forEach((user) => {
        guildMembers.push({
            rank: user.rank,
            name: user.name
        });
    });
}

function render() {
    var element = document.getElementById("__foe_clan_power");
    if(element !== null) {
        element.remove();
    }
    
    var dataDOMElement = document.createElement('div');
    dataDOMElement.id = '__foe_clan_power';
    
    if(currentUserName === '') {
        dataDOMElement.innerHTML = 'Please reload Forge of Empires with DevTools panel on to initiate FOE Guild Helper';
        document.body.appendChild(dataDOMElement);
        return;
    }
    if(Object.keys(storedData).length === 0) {
        dataDOMElement.innerHTML = 'Please visit a guild member to initiate FOE Guild Helper';
        document.body.appendChild(dataDOMElement);
        return;
    }
    
    parts = [];
    
    for(var playerName in storedData) {
        parts.push(guildBuildingsToString(playerName, storedData[playerName]));
    }
    let headerLine = `"name","total power"`;
    
    for (i = 1; i <= maxBuildings; i++) {
        headerLine += `,,"amount of buildings","building type","single building power","age","level"`;
    }

    parts.push(headerLine);
    
    parts.push('');
    parts.push('TODO: ' + renderGuildMembers());
    
    parts.push('');
    parts.push('TOTAL: ' + Object.keys(storedData).length + ' / ' + membersInClan); 
    
    dataDOMElement.innerHTML = parts.reverse().join('<br>');
    
    document.body.appendChild(dataDOMElement);
}

function renderGuildMembers() {
    let parts = [];
    
    guildMembers.forEach((user) => {
        if(user.name in storedData) {
            return;
        }
        parts.push(`${user.name} (#${user.rank})`);
    });
    return parts.join(', ');
}

function parseEntity(entity) {
    if(!entity.connected) {
        return {};
    }
    let allowedEntityIds = {
        Z_MultiAge_CupBonus1: {building: 'HOF', level: 1}, 
        Z_MultiAge_CupBonus1a: {building: 'HOF', level: 1}, 
        Z_MultiAge_CupBonus1b: {building: 'HOF', level: 2}, 
        R_MultiAge_Battlegrounds1: {building: 'SOH', level: 1},
        R_MultiAge_Battlegrounds1a: {building: 'SOH', level: 1},
        R_MultiAge_Battlegrounds1b: {building: 'SOH', level: 2},
        R_MultiAge_Battlegrounds1c: {building: 'SOH', level: 3},
        R_MultiAge_Battlegrounds1d: {building: 'SOH', level: 4},
        R_MultiAge_Battlegrounds1e: {building: 'SOH', level: 5},
        R_MultiAge_Battlegrounds1f: {building: 'SOH', level: 6},
        R_MultiAge_Battlegrounds1g: {building: 'SOH', level: 7},
        R_MultiAge_Battlegrounds1h: {building: 'SOH', level: 8},
    };
    if(!(entity.cityentity_id in allowedEntityIds)) {
        return {};
    }
    let parsedEntity = allowedEntityIds[entity.cityentity_id];
    parsedEntity.age = entity.level + 1;
    return parsedEntity;
}

function guildBuildingsToString(playerName, guildBuildings) {
    let csvParts = [];
    let groupedBuildings = [];
    guildBuildings.forEach((guildBuilding) => {
        if(groupedBuildings[guildBuilding.building] === undefined){
            groupedBuildings[guildBuilding.building] = [];
        }
        if(groupedBuildings[guildBuilding.building][guildBuilding.age] === undefined){
            groupedBuildings[guildBuilding.building][guildBuilding.age] = [];
        }
        if(groupedBuildings[guildBuilding.building][guildBuilding.age][guildBuilding.level] === undefined){
            groupedBuildings[guildBuilding.building][guildBuilding.age][guildBuilding.level] = 0;
        }
        groupedBuildings[guildBuilding.building][guildBuilding.age][guildBuilding.level] += 1;
    });
    
    let totalPower = 0;
    let totalBuildings = 0;
    for(var building in groupedBuildings) {
        for(age in groupedBuildings[building]) {
            for(level in groupedBuildings[building][age]) {
                let power = getPower(building, age, level);
                totalPower += groupedBuildings[building][age][level] * power;
                csvParts.push('');
                csvParts.push(groupedBuildings[building][age][level]);
                csvParts.push(`"${building}"`);
                csvParts.push(power);
                csvParts.push(age);
                csvParts.push(level); 
                totalBuildings++;
            }
        }
    }
    maxBuildings = Math.max(maxBuildings, totalBuildings);

    
    let name = playerName.replace('"', '""');
    
    return `"${name}",${totalPower},` + csvParts.join(','); 
}

function getPower(building, age, level) {
    let powers = {
        HOF: {
            1: {
                2:4,
                3:6,
                4:10,
                5:22,
                6:27,
                7:35,
                8:45,
                9:70,
                10:90,
                11:110,
                12:135,
                13:160,
                14:185,
                15:210,
                16:235,
                17:260,
                18:285,
                19:310
            },
            2: {
                2:6,
                3:9,
                4:15,
                5:35,
                6:44,
                7:57,
                8:74,
                9:118,
                10:153,
                11:188,
                12:232,
                13:276,
                14:320,
                15:364,
                16:408,
                17:452,
                18:496,
                19:540
            }
        },
        SOH: {
            1: {
                2: 60,
                3: 75,
                4: 90,
                5: 105,
                6: 120,
                7: 135,
                8: 150,
                9: 165,
                10: 180,
                11: 195,
                12: 210,
                13: 225,
                14: 240,
                15: 255,
                16: 270,
                17: 285,
                18: 300,
                19: 315,
            },
            2: {
                2: 70,
                3: 88,
                4: 105,
                5: 123,
                6: 140,
                7: 158,
                8: 175,
                9: 193,
                10: 210,
                11: 228,
                12: 245,
                13: 263,
                14: 280,
                15: 298,
                16: 315,
                17: 333,
                18: 350,
                19: 368,
            },
            3: {
                2: 80,
                3: 100,
                4: 120,
                5: 140,
                6: 160,
                7: 180,
                8: 200,
                9: 220,
                10: 240,
                11: 260,
                12: 280,
                13: 300,
                14: 320,
                15: 340,
                16: 360,
                17: 380,
                18: 400,
                19: 420,
            },
            4: {
                2: 90,
                3: 113,
                4: 135,
                5: 158,
                6: 180,
                7: 203,
                8: 225,
                9: 248,
                10: 270,
                11: 293,
                12: 315,
                13: 338,
                14: 360,
                15: 383,
                16: 405,
                17: 428,
                18: 450,
                19: 473,
            },
            5: {
                2: 100,
                3: 125,
                4: 150,
                5: 175,
                6: 200,
                7: 225,
                8: 250,
                9: 275,
                10: 300,
                11: 325,
                12: 350,
                13: 375,
                14: 400,
                15: 425,
                16: 450,
                17: 475,
                18: 500,
                19: 525,
            },
            6: {
                2: 110,
                3: 138,
                4: 166,
                5: 194,
                6: 222,
                7: 250,
                8: 278,
                9: 306,
                10: 334,
                11: 362,
                12: 390,
                13: 418,
                14: 446,
                15: 474,
                16: 502,
                17: 530,
                18: 558,
                19: 586,
            },
            7: {
                2: 120,
                3: 150,
                4: 180,
                5: 210,
                6: 240,
                7: 270,
                8: 300,
                9: 330,
                10: 360,
                11: 390,
                12: 420,
                13: 450,
                14: 480,
                15: 510,
                16: 540,
                17: 570,
                18: 600,
                19: 630,
            },
            8: {
                2: 130,
                3: 163,
                4: 196,
                5: 229,
                6: 262,
                7: 295,
                8: 328,
                9: 361,
                10: 394,
                11: 427,
                12: 460,
                13: 493,
                14: 526,
                15: 559,
                16: 592,
                17: 625,
                18: 658,
                19: 691,
            }
        }
    };
    if(powers[building] === undefined){
        return false;
    }
    if(powers[building][level] === undefined){
        return false;
    }
    if(powers[building][level][age] === undefined){
        return false;
    }
    
    return powers[building][level][age];
}


